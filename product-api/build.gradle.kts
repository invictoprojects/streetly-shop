plugins {
    id("streetly-shop.spring-kotlin-conventions")
}

group = "com.invicto-projects.productapi"
version = "0.0.1-SNAPSHOT"

jib {
    from {
        image = "eclipse-temurin:17-jre-alpine"
    }
    to {
        image = "streetly-shop/product-api"
    }
    container {
        mainClass = "com.invictoprojects.productapi.ProductApiApplicationKt"
    }
}
